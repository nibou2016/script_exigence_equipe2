package pack;

import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterClass;

public class Utilisateur_ne_peut_pas_enregistrer_un_compte_en_double {
	WebDriver driver;

	@Test
	public void Exigence3_Register() throws InterruptedException {

		driver.get("http://tutorialsninja.com/demo/index.php?route=common/home");
		driver.findElement(By.linkText("My Account")).click();
		driver.findElement(By.linkText("Register")).click();
		Thread.sleep(2000);
		driver.findElement(By.id("input-firstname")).sendKeys("sara");
		driver.findElement(By.id("input-lastname")).sendKeys("benmahammed");
		Thread.sleep(2000);
		driver.findElement(By.id("input-email")).sendKeys("sandymillion2021@gmail.com");
		driver.findElement(By.id("input-telephone")).sendKeys("1234567890");
		driver.findElement(By.id("input-password")).sendKeys("12345678");
		Thread.sleep(2000);
		driver.findElement(By.id("input-confirm")).sendKeys("12345678");
		driver.findElement(By.name("agree")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@type='submit']")).click();

	}

	@BeforeClass
	public void setUp() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

	}

	@AfterClass
	public void teardown() {
		 driver.close();
	}

}
